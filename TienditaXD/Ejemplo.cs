﻿using Datos;
using Repository;
using Services.interfaces;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TienditaXD
{
    public class Ejemplo
    {
        private static IProductoServices _productoServices;
        
        public static void CrearProducto() {
            _productoServices = new ProductoServicio(new AplicationDbContext());
            Console.WriteLine("Crear producto");
            Producto producto = new Producto();
            producto = LlenarProducto(producto);
            var response=  _productoServices.Crear(producto);
            Console.WriteLine(response.Message);
        }
        public static Producto LlenarProducto(Producto producto)
        {
            Console.Write("Nombre: ");
            producto.Nombre = Console.ReadLine();
            Console.Write("Descripción: ");
            producto.Descripcion = Console.ReadLine();
            Console.Write("Precio: ");
            producto.Precio = decimal.Parse(Console.ReadLine());
            Console.Write("Costo: ");
            producto.Costo = decimal.Parse(Console.ReadLine());
            Console.Write("Cantidad: ");
            producto.Cantidad = decimal.Parse(Console.ReadLine());
            Console.Write("Tamaño: ");
            producto.Tamano = Console.ReadLine();
            producto.AppUser = "Alfredo xdd";
            producto.IsDeleted = false;
            producto.RowVersion = DateTime.Now;

            return producto;
        }
        public static void ActualizarProducto()
        {
            _productoServices = new ProductoServicio(new AplicationDbContext());
            Console.WriteLine("Editar producto");
            Producto producto = SelecionarProducto();
            producto = LlenarProducto(producto);
            var response = _productoServices.Editar(producto);
            Console.WriteLine(response.Message);
        }
        public static Producto SelecionarProducto()
        {
            _productoServices = new ProductoServicio(new AplicationDbContext());
            var lista = _productoServices.ObtenerLista();
            foreach (Producto producto1 in lista)
            {
                Console.WriteLine(producto1);
            }
            Console.Write("Seleciona el código de producto: ");
            int id = int.Parse(Console.ReadLine());
            Producto produc = _productoServices.BuscarPorId(id);
            if (produc == null)
            {
                SelecionarProducto();
            }
            return produc;
            
        }
        public static void  ListaProductos(){
            _productoServices = new ProductoServicio(new AplicationDbContext());
            var lista = _productoServices.ObtenerLista();
            foreach (Producto producto1 in lista)
            {
                Console.WriteLine(producto1);
            }
           
        }
        public static void ListaXParametros()
        {
            _productoServices = new ProductoServicio(new AplicationDbContext());
            Console.WriteLine("Buscar productos por parametros");
            Console.Write("inserte el parametro a buscar: ");
            string buscar = Console.ReadLine();
            var lista = _productoServices.ObtenerListaPorParametro(buscar);
            foreach (Producto producto1 in lista)
            {
                Console.WriteLine(producto1);
            }
        }
        public static void Eliminar()
        {
            _productoServices = new ProductoServicio(new AplicationDbContext());
            var lista = _productoServices.ObtenerLista();
            foreach (Producto producto1 in lista)
            {
                Console.WriteLine(producto1);
            }
            Console.Write("Seleciona el código de producto que desea eliminar: ");
            int id = int.Parse(Console.ReadLine());
            var response = _productoServices.Eliminar(id);
            Console.WriteLine(response.Message+ "\n \n");
        }



    }
}
