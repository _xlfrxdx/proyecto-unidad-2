﻿using Datos;
using Datos.Util;
using Microsoft.Extensions.Logging;
using Repository;
using Services.interfaces;
using Services.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace TienditaXD
{
    class Program
    {
        
      
        public static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Menu");
                Console.WriteLine("1) Registrar usuario");
                Console.WriteLine("2) Login");
                string opcion = Console.ReadLine();
                ResponseHelper response = new ResponseHelper();
                switch (opcion)
                {
                    case "1":
                        UsuarioMenu.CrearUsuario();
                        break;
                    case "2":
                        response = UsuarioMenu.login();
                        break;

                    case "0": return;
                }

                if (response.Success)
                {
                    MenuPrincipal();
                }
                
            }

        }
        public static void MenuPrincipal()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1) Menú de productos");
            Console.WriteLine("2) Menú de ventas");
            Console.WriteLine("3) Menú de detalle");
            Console.WriteLine("0) Salir");

            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    MenuProducto();
                    break;
                case "2":
                    MenuVentas();
                    break;
                case "3":
                    MenuDetalle();
                    break;
                case "0": return;
            }
            MenuPrincipal();
        }
        public static void MenuProducto()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1) Crear producto");
            Console.WriteLine("2) Buscar productos");
            Console.WriteLine("3) Actualizar producto");
            Console.WriteLine("4) Eliminar producto");
            Console.WriteLine("5) Lista de productos");
            Console.WriteLine("0) Salir");
           
            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    Ejemplo.CrearProducto();
                    break;
                case "2":
                    Ejemplo.ListaXParametros();
                    break;
                case "3":
                     Ejemplo.ActualizarProducto();
                     break;
                case "4":
                    Ejemplo.Eliminar();
                    break;
                case "5":
                    Ejemplo.ListaProductos();
                    break;
                case "0": return;
            }
            MenuProducto();
            
        }
        public static void MenuVentas()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1) Crear venta");
            Console.WriteLine("2) Buscar ventas");
            Console.WriteLine("3) Actualizar venta");
            Console.WriteLine("4) Eliminar venta");
            Console.WriteLine("5) Lista de ventas");
            Console.WriteLine("0) Salir");

            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    VentaMenu.CrearVenta();
                    break;
                case "2":
                    VentaMenu.ListaXParametros();
                    break;
                case "3":
                    VentaMenu.ActualizarVenta();
                    break;
                case "4":
                    VentaMenu.Eliminar();
                    break;
                case "5":
                    VentaMenu.ListaVentas();
                    break;
                case "0": return;
            }
            MenuVentas();

        }
        public static void MenuDetalle()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("1) Crear detalle de la venta");
            Console.WriteLine("2) Buscar ventas");
            Console.WriteLine("3) Actualizar venta");
            Console.WriteLine("4) Eliminar venta");
            Console.WriteLine("5) Lista de ventas");
            Console.WriteLine("0) Salir");

            string opcion = Console.ReadLine();
            switch (opcion)
            {
                case "1":
                    DetalleMenu.CrearDetalle();
                    break;
                case "2":
                    DetalleMenu.ListaXParametros();
                    break;
                case "3":
                    DetalleMenu.ActualizarDetalle();
                    break;
                case "4":
                    DetalleMenu.Eliminar();
                    break;
                case "5":
                    DetalleMenu.ListaDetalles();
                    break;
                case "0": return;
            }
            MenuDetalle();

        }
        //public static bool ReturnL()
        //{
        //    Console.WriteLine("Menu");
        //    Console.WriteLine("1) Registrar usuario");
        //    Console.WriteLine("2) Login");
        //    string opcion = Console.ReadLine();
        //    ResponseHelper response = new ResponseHelper();
        //    switch (opcion)
        //    {
        //        case "1":
        //            UsuarioMenu.CrearUsuario();
        //            break;
        //        case "2":
        //            response = UsuarioMenu.login();
        //            break;

        //        case "0": return;
        //    }

        //    if (response.Success)
        //    {
        //        MenuPrincipal();
        //        return true;
        //    }
        //    return  ;
        //}



    }
}
