﻿using Datos;
using Repository;
using Services.interfaces;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace TienditaXD
{
  
    public class VentaMenu
    {
        private static IVentaServices _ventaServicio;
        public static void CrearVenta()
        {
            _ventaServicio = new VentaServicio( new AplicationDbContext());
            Console.WriteLine("Crear la Venta");
            Venta venta = new Venta();
            venta = LlenarVenta(venta);
            var response = _ventaServicio.Crear(venta);
            Console.WriteLine(response.Message);
        }
        public static Venta LlenarVenta(Venta venta)
        {
            Console.Write("Total: ");
            venta.Total = decimal.Parse(Console.ReadLine());
            venta.Fecha = DateTime.Now;
            Console.Write("Cliente: ");
            venta.Cliente = Console.ReadLine();
            venta.AppUser = "Alfredo xdd";
            venta.IsDeleted = false;
            venta.RowVersion = DateTime.Now;
            return venta;
        }
        public static void ActualizarVenta()
        {
            _ventaServicio = new VentaServicio(new AplicationDbContext());
            Console.WriteLine("Editar producto");
            Venta venta = SelecionarVentas();
            venta = LlenarVenta(venta);
            var response = _ventaServicio.Editar(venta);
            Console.WriteLine(response.Message);
        }
        public static Venta SelecionarVentas()
        {
            _ventaServicio = new VentaServicio(new AplicationDbContext());
            var lista = _ventaServicio.ObtenerLista();
            foreach (Venta venta in lista)
            {
                Console.WriteLine(venta);
            }
            Console.Write("Seleciona el código de la venta: ");
            int id = int.Parse(Console.ReadLine());
            Venta venta1 = _ventaServicio.BuscarPorId(id);
            if (venta1 == null)
            {
                SelecionarVentas();
            }
            return venta1;
        }
        public static void ListaVentas()
        {
            _ventaServicio = new VentaServicio(new AplicationDbContext());
            var lista = _ventaServicio.ObtenerLista();
            foreach (Venta venta1 in lista)
            {
                Console.WriteLine(venta1);
            }
        }
        public static void ListaXParametros()
        {
            _ventaServicio = new VentaServicio(new AplicationDbContext());
            Console.WriteLine("Buscar ventas por nombre del cliente");
            Console.Write("Inserte el nombre a buscar: ");
            string buscar = Console.ReadLine();
            var lista = _ventaServicio.ObtenerListaPorParametro(buscar);
            foreach (Venta venta1 in lista)
            {
                Console.WriteLine(venta1);
            }
        }
        public static void Eliminar()
        {
            _ventaServicio = new VentaServicio(new AplicationDbContext());
            var lista = _ventaServicio.ObtenerLista();
            foreach (Venta venta1 in lista)
            {
                Console.WriteLine(venta1);
            }
            Console.Write("Seleciona el código de la venta que desea eliminar: ");
            int id = int.Parse(Console.ReadLine());
            var response = _ventaServicio.Eliminar(id);
            Console.WriteLine(response.Message + "\n \n");
        }
    }
}
