﻿using Datos;
using Datos.Util;
using Repository;
using Services.interfaces;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TienditaXD
{
    public  class UsuarioMenu
    {
        private static IUserServices _userServices;

        public static ResponseHelper  CrearUsuario()
        {
            _userServices = new UserServicio(new AplicationDbContext());
            Console.WriteLine("Crear usuario \n");
       

            Console.WriteLine("ingrese el usuario \n");
            string username = Console.ReadLine();
            List<string> contraseña = new List<string>();
            Console.WriteLine("ingrese la contraseña \n");
            while (true)
            {
                var tecla = Console.ReadKey(true);
                if (tecla.Key == ConsoleKey.Enter)
                {
                    break;
                }
                else if (tecla.Key == ConsoleKey.Backspace)
                {
                    if (contraseña.Count() > 0)
                    {
                        contraseña.RemoveAt(contraseña.Count - 1);
                        Console.Write("\b \b");
                    }
                }
                else
                {
                    contraseña.Add(Convert.ToString(tecla.KeyChar));
                    Console.Write("*");
                }

            }
            string pass = "";

            for (int i = 0; i < contraseña.Count(); i++)
            {
                pass += contraseña[i];
            }
            var response = _userServices.Crear(username, pass);
            Console.WriteLine(response.Message);
            return response;
        }
        public static ResponseHelper login()
        {
            _userServices = new UserServicio(new AplicationDbContext());
            Console.WriteLine("Inicio de sesión \n");
            Console.WriteLine("ingrese el usuario \n");
            string username = Console.ReadLine();
            List<string> contraseña = new List<string>();
            Console.WriteLine("ingrese la contraseña \n");
            while (true)
            {
                var tecla = Console.ReadKey(true);
                if (tecla.Key == ConsoleKey.Enter)
                {
                    break;
                }
                else if(tecla.Key == ConsoleKey.Backspace){
                    if (contraseña.Count() > 0)
                    {
                        contraseña.RemoveAt(contraseña.Count - 1);
                        Console.Write("\b \b");
                    }
                }
                else
                {
                    contraseña.Add(Convert.ToString(tecla.KeyChar));
                    Console.Write("*");
                }
              
            }
            string pass = "";

            for (int i = 0; i < contraseña.Count(); i++)
            {
                pass += contraseña[i];
            }
            //string pass = Console.ReadLine();
            var response = _userServices.Login(username, pass);
            Console.WriteLine(response.Message);
            return response;
        }
    }
}
