﻿using Datos;
using Datos.ViewModel;
using Repository;
using Services.interfaces;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace TienditaXD
{
    public class DetalleMenu
    {
        private static IDetalleServices _detalleServices;
        //private static IVentaServices _ventaServices;
        //private static IProductoServices _productoServices;
        public static void CrearDetalle()
        {
            _detalleServices = new DetalleServicio(new AplicationDbContext());
            Console.WriteLine("Crear la el detalle");
            Detalle detalle = new Detalle();
            detalle = LlenarDetalle(detalle);
            var response = _detalleServices.Crear(detalle);
            Console.WriteLine(response.Message);
        }
        public static Detalle LlenarDetalle(Detalle detalle)
        {
            Console.WriteLine("Selecciona el codigo del producto para enlazar");
            Ejemplo.ListaProductos();
            detalle.ProductoId = int.Parse(Console.ReadLine());
            Console.WriteLine("\n");
            Console.WriteLine("Selecciona el codigo de la venta para enlazar");
            VentaMenu.ListaVentas();
            Console.WriteLine("\n");
            detalle.VentaId = int.Parse(Console.ReadLine());
            Console.Write("Sub Total: ");
            detalle.Subtotal = decimal.Parse(Console.ReadLine());
            return detalle;
        }
        public static void ActualizarDetalle()
        {
            _detalleServices = new DetalleServicio(new AplicationDbContext());

            Detalle detalle = new Detalle();
            Console.WriteLine("Editar Detalle");
            detalle = SelecionarDetalle();
            detalle = LlenarDetalle(detalle);
            var response = _detalleServices.Editar(detalle);
            Console.WriteLine(response.Message);
        }
        public static Detalle SelecionarDetalle()
        {
            _detalleServices = new DetalleServicio(new AplicationDbContext());

            var lista = _detalleServices.ObtenerLista();
            foreach (Detalle detalle in lista)
            {
                Console.WriteLine(detalle);
            }
            Console.Write("Seleciona el código del detalle: ");
            int id = int.Parse(Console.ReadLine());
            Detalle detall = _detalleServices.BuscarPorId(id);
            if (detall == null)
            {
                SelecionarDetalle();
            }
            return detall;
        }
        public static void ListaDetalles()
        {
            _detalleServices = new DetalleServicio(new AplicationDbContext());

            var lista = _detalleServices.ObtenerLista();
            foreach (Detalle detalle in lista)
            {
                Console.WriteLine($"{detalle.Id}) Nombre cliente:{detalle.Venta.Cliente}, Nombre producto: {detalle.Producto.Nombre} , Subtotal:{detalle.Subtotal} Total:{detalle.Venta.Total}");
            }
        }
        public static void ListaXParametros()
        {
            _detalleServices = new DetalleServicio(new AplicationDbContext());
           
            Console.WriteLine("Buscar ventas por nombre del producto");
            Console.Write("Inserte el nombre a buscar: ");
            string buscar = Console.ReadLine();
            var lista = _detalleServices.ObtenerListaPorParametro(buscar);
            foreach (DetalleProductoViewModel detalle in lista)
            {
                Console.WriteLine($"{detalle.Id}) Nombre cliente:{detalle.NombreCliente}, Nombre producto: {detalle.NombrePro} , Subtotal:{detalle.Subtotal} Total:{detalle.Total}");
            }
        }
        public static void Eliminar()
        {
            _detalleServices = new DetalleServicio(new AplicationDbContext());
            ListaDetalles();

            Console.Write("Seleciona el código de la venta que desea eliminar: ");
            int id = int.Parse(Console.ReadLine());
            var response = _detalleServices.Eliminar(id);
            Console.WriteLine(response.Message + "\n \n");
        }
    }
}
